#!/bin/bash

[ $EUID != 0 ] && { echo "Please run as root." >&2; exit 1; }

kernels=($(cat))

config="/etc/default/${0##*/}"
[ -e "$config" ] && source "$config"

SBDIR="${SBDIR:-/var/sb}"
MOK_KEY="${MOK_KEY:-${SBDIR}/MOK.key}"
MOK_CRT="${MOK_CRT:-${SBDIR}/MOK.crt}"

for kernel in "${kernels[@]}"
do
  if ! read -r pkgbase > /dev/null 2>&1 < "/${kernel%/vmlinuz}/pkgbase"
  then
    # if the kernel has no pkgbase, we skip it
    continue
  fi
  if ! read -r kernelbase > /dev/null 2>&1 < "/${kernel%/vmlinuz}/kernelbase"
  then
    # this kernel has no kernelbase, use pkgbase
    kernelbase="${pkgbase}"
  fi
  kernel="/boot/vmlinuz-${kernelbase}"
  signed="${kernel}.signed"
  # remove old signature if exists
  [ -e "$signed" ] && rm -vf "$signed"
  # sign kernel
  if [ "$1" != "remove" ] && [ -e "$kernel" ]
  then
    echo "Resign Kernel \"${kernel}\""
    sbsign --key "$MOK_KEY" --cert "$MOK_CRT" --output "$signed" "$kernel"
  fi
done
